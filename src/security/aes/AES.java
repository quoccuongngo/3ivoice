package security.aes;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class AES {

	private SecretKeySpec secretKey;
	private byte[] key;

	private byte[] decryptedData;
	private byte[] encryptedData;

	public void setKey(String myKey) {

		MessageDigest sha = null;
		try {
			key = myKey.getBytes("UTF-8");
			System.out.println(key.length);
			sha = MessageDigest.getInstance("SHA-1");
			key = sha.digest(key);
			key = Arrays.copyOf(key, 16); // use only first 128 bit
			secretKey = new SecretKeySpec(key, "AES");

		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public byte[] getDecryptedData() {
		return decryptedData;
	}

	private void setDecryptedData(byte[] decryptedData) {
		this.decryptedData = decryptedData;
	}

	public byte[] getEncryptedData() {
		return encryptedData;
	}

	private void setEncryptedData(byte[] encryptedData) {
		this.encryptedData = encryptedData;
	}

	public byte[] encrypt(byte[] dataToEncrypt) {
		try {
			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			cipher.init(Cipher.ENCRYPT_MODE, secretKey);
			setEncryptedData(cipher.doFinal(dataToEncrypt));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return getEncryptedData();
	}

	public byte[] decrypt(byte[] dataToDecrypt) {
		try {
			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
			cipher.init(Cipher.DECRYPT_MODE, secretKey);
			setDecryptedData(cipher.doFinal(dataToDecrypt));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return getDecryptedData();
	}
}