package org.pjsip.pjsua2.app.sip;

import org.pjsip.pjsua2.OnIncomingCallParam;

public interface OnCallEventListener {
	abstract void onIncomingCall(OnIncomingCallParam prm);
	abstract void onCallStateChanged(MyCall call);
	abstract void onMakeNormalCall(String dest_uri, String name);
	abstract void onMakeSecureCall(String dest_uri, String name);
	abstract void onHangup();
	abstract void onAcceptNormalCall();
	abstract void onAcceptSecureCall();
}
