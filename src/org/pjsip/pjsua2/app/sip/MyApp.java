package org.pjsip.pjsua2.app.sip;

import org.pjsip.pjsua2.AccountConfig;
import org.pjsip.pjsua2.AuthCredInfo;
import org.pjsip.pjsua2.Endpoint;
import org.pjsip.pjsua2.EpConfig;
import org.pjsip.pjsua2.TransportConfig;
import org.pjsip.pjsua2.pjsip_transport_type_e;

import android.util.Log;

public class MyApp {

	//Firstly, load the pjsua2 library 
	static {
		System.loadLibrary("pjsua2");
	}
	
	public static final int PJSIP_REGISTER_SUCCESS = 1;
	public static final int PJSIP_REGISTER_UNSUCCESS = 0;
	public static final int PJSIP_INIT_SUCCESS = 1;
	public static final int PJSIP_INIT_UNSUCCESS = 0;
	
	public static OnRegisterEventListener onRegisterEventListener;
	public static OnCallEventListener onCallEventListener;
	public static Endpoint ep = new Endpoint();
	public static String aesPass = "3ivoice";
	public MyAccount acc;
	
	private EpConfig epCfg = new EpConfig();
	private TransportConfig sipTpCfg = new TransportConfig();
	private final int SIP_PORT = 5060;
	
	public int init(OnCallEventListener callEventListener,
			OnRegisterEventListener registerEventListener){
		MyApp.onCallEventListener = callEventListener;
		MyApp.onRegisterEventListener = registerEventListener;
		
		/* Create endpoint */
		try {
			ep.libCreate();
		} catch (Exception e) {
			Log.d("MyApp", "Can not create endpoint");
			e.printStackTrace();
			return MyApp.PJSIP_INIT_UNSUCCESS;
		}
		
		sipTpCfg.setPort(SIP_PORT);
		epCfg.getUaConfig().setMaxCalls(1);
		try {
			ep.libInit(epCfg);
		} catch (Exception e) {
			Log.d("MyApp", "Can not init library");
			e.printStackTrace();
			return MyApp.PJSIP_INIT_UNSUCCESS;
		}
		
		try {
			ep.transportCreate(pjsip_transport_type_e.PJSIP_TRANSPORT_UDP, sipTpCfg);
		} catch (Exception e) {
			Log.d("MyApp", "Can not create transport");
			e.printStackTrace();
			return MyApp.PJSIP_INIT_UNSUCCESS;
		}
		try {
			ep.libStart();
		} catch (Exception e) {
			Log.d("MyApp", "Can not start library");
			e.printStackTrace();
			return MyApp.PJSIP_INIT_UNSUCCESS;
		}
		
		return MyApp.PJSIP_INIT_SUCCESS;
	}
	
	public int registerAccount(String uri, String pass) {
		
		AccountConfig acfg = new AccountConfig();
		String user = uri.substring(0, uri.indexOf('@'));
		String server = uri.substring(uri.indexOf('@') + 1);
		acfg.setIdUri("sip:" + user + "@" + server);
		acfg.getRegConfig().setRegistrarUri("sip:" + server);
		AuthCredInfo cred = new AuthCredInfo("digest", "*", user, 0, pass);
		acfg.getSipConfig().getAuthCreds().add(cred);
		this.acc = new MyAccount(acfg);
		try {
			this.acc.create(acfg);
			int code = 0;
			//trying
			do{
				code = this.acc.getInfo().getRegStatus().swigValue();
			} while(code < 200);
			//failed
			if (code >= 400) {
				this.acc = null;
				return MyApp.PJSIP_REGISTER_UNSUCCESS;
			}
		} catch (Exception e) {
			this.acc = null;
			e.printStackTrace();
			return MyApp.PJSIP_REGISTER_UNSUCCESS;
		}
		
		//success
		return MyApp.PJSIP_REGISTER_SUCCESS;
	}
	
	public void unregisterAccount(){
		this.acc.delete();
	}
	
	public void deinit() {
		/* Try force GC to avoid late destroy of PJ objects as they should be
		 * deleted before lib is destroyed
		 */
		Runtime.getRuntime().gc();
		
		/* Shutdown pjsua. Note that Endpoint destructor will also invoke
		 * libDestroy(), so this will be a test of double libDestroy().
		 */
		try {
			ep.libDestroy();
		} catch (Exception e) {}
		
		/* Force delete Endpoint here, to avoid deletion from a non-
		 * registered thread (by GC?). 
		 */
		ep.delete();
		ep = null;
	}
}
