package org.pjsip.pjsua2.app.sip;

import org.pjsip.pjsua2.AccountConfig;
import org.pjsip.pjsua2.CallInfo;
import org.pjsip.pjsua2.CallOpParam;
import org.pjsip.pjsua2.OnIncomingCallParam;
import org.pjsip.pjsua2.OnRegStateParam;
import org.pjsip.pjsua2.pjsip_inv_state;
import org.pjsip.pjsua2.pjsip_status_code;
import org.pjsip.pjsua2.app.ui.CallingActivity;
import org.pjsip.pjsua2.app.ui.IncomingCallActivity;
import org.pjsip.pjsua2.app.ui.LoginActivity;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.IBinder;
import android.os.Message;
import android.widget.Toast;

public class SipService extends Service
		implements OnCallEventListener, OnRegisterEventListener, Callback {
	public static Handler handler_;
	private final Handler handler = new Handler(this);
	
	public static MyApp app = null;
	public static MyCall currentCall = null;
	public static MyAccount account = null;
	public static AccountConfig accCfg = null;
	
	//Information of incoming call
	private String name;
	private String uri;
	
	public static String remoteIP = null;
	public static String localIP = null;
	@Override
	public void onCreate(){
//		MyApp.onCallEventListener = this;
//		MyApp.onRegisterEventListener = this;
		handler_ = handler;
		
		if (app != null) {
			app.deinit();
		}
		app = new MyApp();
		if (app.init(this, this) == MyApp.PJSIP_INIT_UNSUCCESS) {
			
		}
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId){
		return Service.START_NOT_STICKY;
	}
	
	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}
	
	@Override
	public void onDestroy() {
		try{
			app.deinit();
		} catch(Exception e) {
			e.printStackTrace();
		}
		handler_ = null;
	}
	
	@Override
	public void onLogin(String uri, String pass) {
		int register_result = app.registerAccount(uri, pass);
		if (register_result == MyApp.PJSIP_REGISTER_SUCCESS) {
			account = app.acc;
		}
		else {
			account = null;
		}
	}

	@Override
	public void onLogout(){
		account.delete();
		account = null;
		Intent intent = new Intent(this, LoginActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
	}
	
	@Override
	public void onRegStateChanged(OnRegStateParam prm) {		
		int expiration = prm.getExpiration();
		pjsip_status_code code = prm.getCode();
		if (expiration == 0) {
			//"Unregistration";
			if (code.swigValue() == 200) {
				//"successful";
				Intent intent = new Intent(this, LoginActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
			}
		}
	}

	@Override
	public void onIncomingCall(OnIncomingCallParam iprm) {
		MyCall incall = new MyCall(account, iprm.getCallId(), MyCall.CALL_UNKNOW_METHOD);
		final MyCall call = incall;
		CallOpParam prm = new CallOpParam();
		if (currentCall != null){
			prm.setStatusCode(pjsip_status_code.PJSIP_SC_BUSY_HERE);
			try {
				call.hangup(prm);
			} catch (Exception e) {}
			call.delete();
			return;
		}
		
		/* Answer with ringing */
		prm.setStatusCode(pjsip_status_code.PJSIP_SC_RINGING);
		try {
			call.answer(prm);
		} catch (Exception e) {}
		
		SipService.currentCall = call;
		name = new String("unknown");
		uri = new String("unknown");
		try {
			name = call.getCallerName();
			uri = call.getCallerUri();	
		} catch (Exception e) {
		}
		
		//Start incoming call activity
		Intent i = new Intent(this, IncomingCallActivity.class);
		i.putExtra("name", name);
		i.putExtra("uri", uri);
		i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		getApplication().startActivity(i);
	}

	@Override
	public void onMakeSecureCall(String dest_uri, String name){
		/* Only one call at anytime */
		if (currentCall != null) {
			return;
		}
		MyCall call = new MyCall(account, MyCall.CALL_SECURE);
		CallOpParam prm = new CallOpParam(true);
		
		try {
			call.makeSecureCall("sip:"+dest_uri, prm);
		} catch (Exception e) {
			Context ctx = getApplicationContext();
			Toast.makeText(ctx, "Error happened while making a new call", Toast.LENGTH_LONG)
				.show();
			call.delete();
			return;
		}
		
		currentCall = call;
		currentCall.setMethodCode(MyCall.CALL_SECURE);
		//Show calling activity
		Intent i = new Intent(this, CallingActivity.class);
		i.putExtra("name", name);
		i.putExtra("uri", dest_uri);
		i.putExtra("state", "Calling (Encrypted call)");
		i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		getApplication().startActivity(i);
	}
	
	@Override
	public void onMakeNormalCall(String dest_uri, String name){
		/* Only one call at anytime */
		if (currentCall != null) {
			return;
		}
		MyCall call = new MyCall(account, MyCall.CALL_NORMAL);
		CallOpParam prm = new CallOpParam(true);
		
		try {
			call.makeNormalCall("sip:"+dest_uri, prm);
		} catch (Exception e) {
			call.delete();
			Context ctx = getApplicationContext();
			Toast.makeText(ctx, "Error happened while making a new call", Toast.LENGTH_LONG)
				.show();
			return;
		}
		currentCall = call;
		currentCall.setMethodCode(MyCall.CALL_NORMAL);
		//Show calling activity
		Intent i = new Intent(this, CallingActivity.class);
		i.putExtra("name", name);
		i.putExtra("uri", dest_uri);
		i.putExtra("state", "Calling (Unencrypted call)");
		i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		getApplication().startActivity(i);
	}
	
	@Override
	public void onHangup() {
		if (currentCall != null) {
			CallOpParam prm = new CallOpParam();
			prm.setStatusCode(pjsip_status_code.PJSIP_SC_DECLINE);
			try {
				currentCall.hangup(prm);
			} catch (Exception e) {
			}
			currentCall = null;
		}
	}
	
	@Override
	public void onAcceptNormalCall(){
		CallOpParam prm = new CallOpParam();
		prm.setStatusCode(pjsip_status_code.PJSIP_SC_OK);
		try {
			currentCall.setMethodCode(MyCall.CALL_NORMAL);
			currentCall.answer(prm);
		} catch (Exception e) {
		}
		//Show calling activity
		Intent i = new Intent(this, CallingActivity.class);
		i.putExtra("name", name);
		i.putExtra("uri", uri);
		i.putExtra("state", "Connected (Unencrypted call)");
		i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		getApplication().startActivity(i);
	}
	
	@Override
	public void onAcceptSecureCall(){
		CallOpParam prm = new CallOpParam();
		prm.setStatusCode(pjsip_status_code.PJSIP_SC_OK);
		try {
			currentCall.setMethodCode(MyCall.CALL_SECURE);
			currentCall.answer(prm);
		} catch (Exception e) {
		}
		//Show calling activity
		Intent i = new Intent(this, CallingActivity.class);
		i.putExtra("name", name);
		i.putExtra("uri", uri);
		i.putExtra("state", "Connected (Encrypted call)");
		i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		getApplication().startActivity(i);
	}
	
	@Override
	public void onCallStateChanged(MyCall call) {
		if (	currentCall == null ||
				call.getId() != currentCall.getId()) {
			return;
		}
		CallInfo ci;
		try {
			ci = call.getInfo();
		} catch (Exception e) {
			ci = null;
		}
		if (ci != null &&
				ci.getState() == pjsip_inv_state.PJSIP_INV_STATE_CONFIRMED) {
			Message m = Message.obtain(SipService.handler_, MyCall.PJSIP_INV_STATE_CONFIRMED);
			m.sendToTarget();
		}
		if (ci != null &&
				ci.getState() == pjsip_inv_state.PJSIP_INV_STATE_DISCONNECTED) {
			currentCall = null;
		}
	}

	@Override
	public boolean handleMessage(Message m) {
		// TODO Auto-generated method stub
		if (m.what == MyCall.MSG_HANGUP_CALL) {
			if (CallingActivity.handler_ != null) {
				Message m2 = Message.obtain(CallingActivity.handler_, MyCall.MSG_HANGUP_CALL);
				m2.sendToTarget();
			}
			if (IncomingCallActivity.handler_ != null) {
				Message m2 = Message.obtain(IncomingCallActivity.handler_, MyCall.MSG_HANGUP_CALL);
				m2.sendToTarget();
			}
			return true;
		}
		else if (m.what == MyCall.PJSIP_INV_STATE_CONFIRMED) {
			if (CallingActivity.handler_ != null) {
				Message m2 = Message.obtain(CallingActivity.handler_, MyCall.PJSIP_INV_STATE_CONFIRMED);
				m2.sendToTarget();
			}
			return true;
		}
		return false;
	}
}
