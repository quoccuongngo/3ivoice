package org.pjsip.pjsua2.app.sip;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;

import org.pjsip.pjsua2.AudioMedia;
import org.pjsip.pjsua2.Call;
import org.pjsip.pjsua2.CallInfo;
import org.pjsip.pjsua2.CallMediaInfo;
import org.pjsip.pjsua2.CallMediaInfoVector;
import org.pjsip.pjsua2.CallOpParam;
import org.pjsip.pjsua2.Media;
import org.pjsip.pjsua2.OnCallMediaStateParam;
import org.pjsip.pjsua2.OnCallStateParam;
import org.pjsip.pjsua2.pjmedia_type;
import org.pjsip.pjsua2.pjsip_inv_state;
import org.pjsip.pjsua2.pjsua_call_media_status;

import security.aes.AES;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaRecorder;
import android.os.Message;

public class MyCall extends Call {
	public static final int MSG_HANGUP_CALL = 10;
	public static final int PJSIP_INV_STATE_CONFIRMED = 11;
	public static int CALL_UNKNOW_METHOD = 0;
	public static int CALL_SECURE = 100;
	public static int CALL_NORMAL = 200;
	public static boolean isCalling;
	
	private int callMethod;
	
	public MyCall(MyAccount acc, int call_id, int method){
		super(acc, call_id);
		this.callMethod = method;
	}
	
	public MyCall(MyAccount acc, int method){
		super(acc);
		this.callMethod = method;
	}

	@Override
	public void onCallState(OnCallStateParam prm) {
		MyApp.onCallEventListener.onCallStateChanged(this);
		
		//If the Call is disconnected, delete immediately
		try {
			CallInfo ci = this.getInfo();
			if (ci.getState() == pjsip_inv_state.PJSIP_INV_STATE_DISCONNECTED){
				this.delete();
				//Finish calling or incoming call activity
				Message m = Message.obtain(SipService.handler_, MSG_HANGUP_CALL);
				m.sendToTarget();
			}
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
	}
	
	public void makeSecureCall(String dest_uri, CallOpParam prm) throws Exception {		
		super.makeCall(dest_uri, prm);
	}
	
	public void makeNormalCall(String dest_uri, CallOpParam prm) throws Exception {
		super.makeCall(dest_uri, prm);
	}
	
	public String getCallerName() throws Exception{
		String temp = this.getInfo().getRemoteContact();
		String name = temp.substring(1, temp.indexOf('<')-2);
		return name;
	}
	public String getCallerUri() throws Exception{
		String temp =  this.getInfo().getRemoteUri();
		String uri = temp.substring(temp.indexOf(':')+1, temp.length()-1);
		return uri;
	}
	public String getMethod() {
		if (this.callMethod == MyCall.CALL_NORMAL){
			return "(Normal)";
		}
		else if (this.callMethod == MyCall.CALL_SECURE){
			return "(Encrypted)";
		}
		else
			return "(Unknown)";
	}
	public int getMethodCode() {
		return this.callMethod;
	}
	public void setMethodCode(int method_code){
		if (method_code == MyCall.CALL_NORMAL) {
			this.callMethod = MyCall.CALL_NORMAL;
		} else if (method_code == MyCall.CALL_SECURE) {
			this.callMethod = MyCall.CALL_SECURE;
		} else {
			this.callMethod = MyCall.CALL_UNKNOW_METHOD;
		}
	}

	@Override
	public void onCallMediaState(OnCallMediaStateParam prm) {
		CallInfo ci;
		try {
			ci = getInfo();
		} catch (Exception e) {
			return;
		}
		
		if ((this.getMethodCode() == MyCall.CALL_SECURE)) {
			CallMediaInfoVector cmiv = ci.getMedia();
			for (int i = 0; i < cmiv.size(); i++) {
				CallMediaInfo cmi = cmiv.get(i);
				if (cmi.getType() == pjmedia_type.PJMEDIA_TYPE_AUDIO &&
						((cmi.getStatus() == pjsua_call_media_status.PJSUA_CALL_MEDIA_ACTIVE) ||
						(cmi.getStatus() == pjsua_call_media_status.PJSUA_CALL_MEDIA_REMOTE_HOLD))){
							MyCall.isCalling = true;
							String s;
							try {
								s = this.getStreamInfo(i).getRemoteRtpAddress();
								String remoteIP = s.substring(0, s.indexOf(":"));
								SipService.remoteIP = remoteIP;
								new ReceiverServerThread(SipService.localIP).start();
								new SenderClientThread(SipService.remoteIP).start();
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
				}		
			}
		}
		
		else if ((this.getMethodCode() == MyCall.CALL_NORMAL)) {
			CallMediaInfoVector cmiv = ci.getMedia();
			for (int i = 0; i < cmiv.size(); i++) {
				CallMediaInfo cmi = cmiv.get(i);
				if (cmi.getType() == pjmedia_type.PJMEDIA_TYPE_AUDIO &&
						((cmi.getStatus() == pjsua_call_media_status.PJSUA_CALL_MEDIA_ACTIVE) ||
						(cmi.getStatus() == pjsua_call_media_status.PJSUA_CALL_MEDIA_REMOTE_HOLD))) {
					Media m = getMedia(i);
					AudioMedia am = AudioMedia.typecastFromMedia(m);
					
					try {
						MyApp.ep.audDevManager().getCaptureDevMedia().startTransmit(am);
						am.startTransmit(MyApp.ep.audDevManager().getPlaybackDevMedia());
					} catch (Exception e) {
						continue;
					}
				}
			}
		}
	}
}

class SenderClientThread extends Thread {
	// get a datagram socket
	private InetAddress address;
    private DatagramSocket socket;
    private AES encrypter;
    private int min;
    private AudioRecord recorder;
    
	public SenderClientThread(String destIP) throws SocketException, UnknownHostException {
		this.address = InetAddress.getByName(destIP);
		socket = new DatagramSocket();
		encrypter = new AES();
		encrypter.setKey(MyApp.aesPass);
		min = AudioRecord.getMinBufferSize(8000, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT);
		recorder = new AudioRecord(MediaRecorder.AudioSource.VOICE_COMMUNICATION, 8000, AudioFormat.CHANNEL_IN_MONO,
				AudioFormat.ENCODING_PCM_16BIT, min);
	}
	@Override
	public void run() {
		recorder.startRecording();
		while(MyCall.isCalling) {
			try {
				byte[] data = new byte[16];
				recorder.read(data, 0, data.length);
				byte[] sendData = new byte[32];
				sendData = encrypter.encrypt(data);
				DatagramPacket packet = new DatagramPacket(sendData, sendData.length, this.address, 8484);
				socket.send(packet);
			} catch (IOException e) {
				continue;
			}
		}
		socket.close();
		recorder.stop();
		recorder.release();
	}
}

class ReceiverServerThread extends Thread {
	private DatagramSocket socket = null;
	private AES decrypter;
	private int maxJitter;
	private AudioTrack track;
	private InetAddress address;

	public ReceiverServerThread(String ip) throws SocketException, UnknownHostException {
		address = InetAddress.getByName(ip);
		socket = new DatagramSocket(8484, address);
		decrypter = new AES();
		decrypter.setKey(MyApp.aesPass);
		maxJitter = AudioTrack.getMinBufferSize(8000, AudioFormat.CHANNEL_OUT_MONO, AudioFormat.ENCODING_PCM_16BIT);
		track = new AudioTrack(AudioManager.STREAM_VOICE_CALL, 8000, AudioFormat.CHANNEL_OUT_MONO, AudioFormat.ENCODING_PCM_16BIT,
						maxJitter, AudioTrack.MODE_STREAM);
	}
	
	@Override
	public void run() {
		track.play();
		MyCall.isCalling = true;
		while(MyCall.isCalling){
			try{
				byte[] receiveData = new byte[32];
				
				//receive request
				DatagramPacket packet = new DatagramPacket(receiveData, receiveData.length);
				socket.receive(packet);
				decrypter.decrypt(receiveData);
				track.write(decrypter.getDecryptedData(), 0, 16);
			}
			catch (IOException e){
				continue;
				
			}
		}
		socket.close();
		track.stop();
		track.release();
	}
}
