package org.pjsip.pjsua2.app.sip;

import org.pjsip.pjsua2.Account;
import org.pjsip.pjsua2.AccountConfig;
import org.pjsip.pjsua2.OnIncomingCallParam;
import org.pjsip.pjsua2.OnRegStateParam;

public class MyAccount extends Account {
	public AccountConfig config;
	
	MyAccount(AccountConfig cfg){
		super();
		this.config = cfg;
	}
	
	@Override
	public void onRegState(OnRegStateParam prm){
		MyApp.onRegisterEventListener.onRegStateChanged(prm);
	}
	
	@Override
	public void onIncomingCall(OnIncomingCallParam prm) {
		MyApp.onCallEventListener.onIncomingCall(prm);
	}
}
