package org.pjsip.pjsua2.app.sip;

import org.pjsip.pjsua2.OnRegStateParam;

public interface OnRegisterEventListener {
	abstract void onLogin(String uri, String pass);
	
	//Callee when register state changed, include logout
	abstract void onRegStateChanged(OnRegStateParam prm);

	abstract void onLogout();
}
