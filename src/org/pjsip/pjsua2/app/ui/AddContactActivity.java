package org.pjsip.pjsua2.app.ui;

import org.pjsip.pjsua2.app.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class AddContactActivity extends Activity {
	private EditText editName;
	private EditText editUri;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_contact);
		
		editName = (EditText)findViewById(R.id.editName);
		editUri = (EditText)findViewById(R.id.editUri);
	}
	
	public void onCancel(View v) {
		this.finish();
	}
	
	public void onAdd(View v) {
		Intent data = new Intent();
		data.putExtra("name", editName.getText().toString());
		
		if (!isValidUri()){
			Toast
				.makeText(AddContactActivity.this, "Uri is not valid", Toast.LENGTH_LONG)
				.show();
			return;
		}
		data.putExtra("uri", editUri.getText().toString());
		setResult(RESULT_OK, data);
		
		finish();
	}
	
	private boolean isValidUri() {
		String uri = editUri.getText().toString();
		if (uri.equals(null)){
			return false;
		}
		if (uri.indexOf('@') <= 0) {
			return false;
		}
		return true;
	}
}
