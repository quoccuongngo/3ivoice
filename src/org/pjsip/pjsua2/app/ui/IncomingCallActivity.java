package org.pjsip.pjsua2.app.ui;

import org.pjsip.pjsua2.app.R;
import org.pjsip.pjsua2.app.sip.MyApp;
import org.pjsip.pjsua2.app.sip.MyCall;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.view.View;
import android.widget.TextView;

public class IncomingCallActivity extends Activity implements Callback {
	public static Handler handler_;
	private final Handler handler = new Handler(this);
	
	String name;
	String uri;
	MediaPlayer mp;
	
	private TextView txtCaller;
	private TextView txtCallerUri;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_incoming_call);
		handler_ = handler;
		
		txtCaller = (TextView)findViewById(R.id.txtCaller);
		txtCallerUri = (TextView)findViewById(R.id.txtCallerUri);
		
		name = this.getIntent().getStringExtra("name");
		uri = this.getIntent().getStringExtra("uri");
		txtCaller.setText(name);
		txtCallerUri.setText(uri);
		
		mp = MediaPlayer.create(this, R.raw.old_telephone);
		mp.setLooping(true);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		mp.start();
	}
	
	@Override
	protected void onDestroy() {
		if (mp.isPlaying()) {
			mp.stop();
		}
		mp.release();
		handler_ = null;
		super.onDestroy();
	}
	
	public void onDecline(View v){
		MyApp.onCallEventListener.onHangup();
		handler_ = null;
		this.finish();
	}
	
	public void onAnswerNormalCall(View v){
		MyApp.onCallEventListener.onAcceptNormalCall();
		this.finish();
	}
	
	public void onAnswerSecureCall(View v){
		MyApp.onCallEventListener.onAcceptSecureCall();
		this.finish();
	}

	@Override
	public boolean handleMessage(Message m) {
		if (m.what == MyCall.MSG_HANGUP_CALL) {
			this.finish();
			return true;
		}
		return false;
	}
}
