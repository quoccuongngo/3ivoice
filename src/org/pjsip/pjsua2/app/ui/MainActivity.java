package org.pjsip.pjsua2.app.ui;

import java.util.ArrayList;
import java.util.List;

import org.pjsip.pjsua2.app.R;
import org.pjsip.pjsua2.app.sip.MyApp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends Activity {
	List<Contact> model = new ArrayList<Contact>();
	ContactAdapter adapter = null;
	
	//Request code for add new contact to the list
	final int request_code_add = 1;
	
	ListView lstContact;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		lstContact = (ListView)findViewById(R.id.lstContact);
		lstContact.setOnItemClickListener(onContactClick);
		adapter = new ContactAdapter(this, model);
		lstContact.setAdapter(adapter);
	}
	
	private OnItemClickListener onContactClick = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, 
				View view, int position, long id) {
			Contact ct = model.get(position);
			
			Intent i = new Intent(MainActivity.this, MakeCallActivity.class);
			i.putExtra("uri", ct.getUri());
			i.putExtra("name", ct.getName());
			startActivity(i);
		}
	};
	
	@Override
	public void onActivityResult(int requestCode, 
			int responceCode, Intent data){
		if (requestCode == request_code_add) {
			if (responceCode == RESULT_OK) {
				String name = data.getStringExtra("name");
				String uri = data.getStringExtra("uri");
				Contact ct = new Contact(name, uri);
				adapter.insert(ct, 0);
			}
		}
	}
	
	public void onAddContact(View v){
		Intent i = new Intent(MainActivity.this, AddContactActivity.class);
		startActivityForResult(i, request_code_add);
	}
	public void onLogout(View v){
		AlertDialog.Builder dlg = new AlertDialog.Builder(MainActivity.this);
		dlg.setTitle("Logout");
		dlg.setMessage("Do you want to logout?");
		dlg.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				MyApp.onRegisterEventListener.onLogout();
				MainActivity.this.finish();
			}
		});
		dlg.setNegativeButton("No", null);
		dlg.show();
	}
	
}

class Contact {
	private String name;
	private String uri;
	
	public Contact(String name, String uri) {
		this.name = name;
		this.uri = uri;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}
}

class ContactHolder{
	private TextView txtName;
	private TextView txtUri;
	
	public ContactHolder(View row){
		txtName = (TextView)row.findViewById(R.id.txtName);
		txtUri = (TextView)row.findViewById(R.id.txtUri);
	}
	
	public void populateFrom(Contact contact) {
		txtName.setText(contact.getName());
		txtUri.setText(contact.getUri());
	}
}

class ContactAdapter extends ArrayAdapter<Contact> {
	private final List<Contact> model;
	private final Context context;
	
	public ContactAdapter(Context context, List<Contact> model) {
		super(context, android.R.layout.simple_list_item_1, model);
		this.model = model;
		this.context = context;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		ContactHolder holder = null;
		
		if (row == null) {
			LayoutInflater inflater = (LayoutInflater)
					context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			row = inflater.inflate(R.layout.row, null);
			holder = new ContactHolder(row);
			row.setTag(holder);
		}
		else {
			holder = (ContactHolder)row.getTag();
		}
		
		holder.populateFrom(model.get(position));
		
		return row;
	}
}
