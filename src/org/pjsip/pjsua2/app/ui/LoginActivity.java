package org.pjsip.pjsua2.app.ui;

import org.pjsip.pjsua2.app.R;
import org.pjsip.pjsua2.app.sip.MyApp;
import org.pjsip.pjsua2.app.sip.SipService;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.text.format.Formatter;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class LoginActivity extends Activity {
	EditText etUser = null;
	EditText etPass = null;
	Button btnLogin = null;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		etUser = (EditText)findViewById(R.id.editID);
		etPass = (EditText)findViewById(R.id.editPass);
		btnLogin = (Button)findViewById(R.id.btnLogin);
		btnLogin.setOnClickListener(onLogin);

		//Start SipService to init endpoint
		Intent intent = new Intent(this, SipService.class);
		startService(intent);
		
		WifiManager wm = (WifiManager) getSystemService(WIFI_SERVICE);
		SipService.localIP = Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());
	}
	
	private OnClickListener onLogin = new OnClickListener() {
		
		@Override
		public void onClick(View arg0) {
			String uri = etUser.getText().toString();
			String pass = etPass.getText().toString();
			
			MyApp.onRegisterEventListener.onLogin(uri, pass);
			
			//login failed
			if (SipService.account == null) {
				new AlertDialog.Builder(LoginActivity.this)
					.setTitle("Login failed")
					.setMessage("Please check you uri, password and connection")
					.setPositiveButton(android.R.string.ok, null)
					.show();
				return;
			}
			//login successful
			else {
				LoginActivity.this.finish();
				Intent i = new Intent(LoginActivity.this, MainActivity.class);
				startActivity(i);
			}
		}
	};
	
	public void onLogin(View v) {
		String uri = etUser.getText().toString();
		String pass = etPass.getText().toString();
		
		MyApp.onRegisterEventListener.onLogin(uri, pass);
		
		//login failed
		if (SipService.account == null) {
			new AlertDialog.Builder(this)
				.setTitle("Login failed")
				.setMessage("Please check you uri, password and connection")
				.setPositiveButton(android.R.string.ok, null)
				.show();
			return;
		}
		//login successful
		else {
			this.finish();
			Intent i = new Intent(this, MainActivity.class);
			startActivity(i);
		}
	}
}
