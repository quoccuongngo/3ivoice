package org.pjsip.pjsua2.app.ui;

import org.pjsip.pjsua2.app.R;
import org.pjsip.pjsua2.app.sip.MyApp;
import org.pjsip.pjsua2.app.sip.MyCall;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class CallingActivity extends Activity implements Callback {
	AudioManager am = null;
	
	public static Handler handler_;
	private final Handler handler = new Handler(this);
	
	private TextView tvName;
	private TextView tvUri;
	private TextView tvState;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_calling);
		
		am = (AudioManager)this.getSystemService(Context.AUDIO_SERVICE);
		am.setMode(AudioManager.MODE_IN_COMMUNICATION);
		
		handler_ = handler;
		
		tvName = (TextView)findViewById(R.id.txtCalleeName);
		tvUri = (TextView)findViewById(R.id.txtCalleeUri);
		tvState = (TextView)findViewById(R.id.txtState);
		
		Intent i = this.getIntent();
		tvName.setText(i.getStringExtra("name").toString());
		tvUri.setText(i.getStringExtra("uri").toString());
		tvState.setText(i.getStringExtra("state").toString());
	}
	
	@Override
    protected void onDestroy() {
		MyCall.isCalling = false;
    	super.onDestroy();
    	handler_ = null;
    }
	
	public void onEndCall(View v){
		handler_ = null;
		MyApp.onCallEventListener.onHangup();
		this.finish();
	}

	boolean isSpeaker = false;
	public void onChangePlaybackDevice(View v){
		Button btnSpeaker = (Button) findViewById(R.id.btnChangeDevice);
		if (isSpeaker == true) {
			am.setSpeakerphoneOn(false);
			isSpeaker = false;
			btnSpeaker.setBackgroundResource(R.drawable.bkg_speaker_off);
		} else {
			am.setSpeakerphoneOn(true);
			isSpeaker = true;
			btnSpeaker.setBackgroundResource(R.drawable.bkg_speaker_on);
		}
	}
	
	@Override
	public boolean handleMessage(Message m) {
		if (m.what == MyCall.MSG_HANGUP_CALL) {
			this.finish();
			return true;
		}
		else if (m.what == MyCall.PJSIP_INV_STATE_CONFIRMED) {
			String str = tvState.getText().toString();
			tvState.setText("Connected "
					+ str.substring(str.indexOf('(')));
		}
		return false;
	}
}