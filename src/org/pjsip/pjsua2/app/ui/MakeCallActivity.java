package org.pjsip.pjsua2.app.ui;

import org.pjsip.pjsua2.app.R;
import org.pjsip.pjsua2.app.sip.MyApp;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;

public class MakeCallActivity extends Activity {
	String uri = null;
	String name = null;
	
	LinearLayout normal_call;
	LinearLayout secure_call;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_call);
		
		normal_call = (LinearLayout)findViewById(R.id.normal_call);
		secure_call = (LinearLayout)findViewById(R.id.secure_call);
		normal_call.setOnClickListener(onNormalCall);
		secure_call.setOnClickListener(onSecureCall);
		
		uri = this.getIntent().getStringExtra("uri");
		name = this.getIntent().getStringExtra("name");
	}
	
	private OnClickListener onNormalCall = new OnClickListener() {
		@Override
		public void onClick(View arg0) {
			MyApp.onCallEventListener.onMakeNormalCall(uri, name);
			MakeCallActivity.this.finish();
		}
	};
	private OnClickListener onSecureCall = new OnClickListener() {
		
		@Override
		public void onClick(View arg0) {
			MyApp.onCallEventListener.onMakeSecureCall(uri, name);
			MakeCallActivity.this.finish();
		}
	};
}
